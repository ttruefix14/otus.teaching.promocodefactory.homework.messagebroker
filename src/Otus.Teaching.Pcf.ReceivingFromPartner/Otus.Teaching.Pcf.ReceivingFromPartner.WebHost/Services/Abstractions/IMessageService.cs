﻿using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services.Abstractions
{
    public interface IMessageService
    {
        Task PublishMessage<T>(T message);
    }
}

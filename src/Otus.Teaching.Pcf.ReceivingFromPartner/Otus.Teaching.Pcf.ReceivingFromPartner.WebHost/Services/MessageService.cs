﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Options;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services.Abstractions;
using RabbitMQ.Client;
using System;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services
{
    public class MessageService : IMessageService
    {
        private readonly string _exchange = "Pcf.ReceivingFromPartner.Promocodes";
        private readonly string _routingKey = "Pcf.ReceivingFromPartner.Promocode";

        private readonly ConnectionFactory _connectionFactory;
        public MessageService(IOptions<BusOptions> busOptions)
        {
            _connectionFactory = new()
            {
                UserName = busOptions.Value.Username,
                Password = busOptions.Value.Password,
                HostName = busOptions.Value.Host,
                Port = busOptions.Value.Port,
                VirtualHost = busOptions.Value.VirtualHost
            };

            using var con = _connectionFactory.CreateConnection();
            if (con.IsOpen)
            {
                using var channel = con.CreateModel();
                // Задаем Exchange куда будем поставлять все сообщения
                channel.ExchangeDeclare(
                    exchange: _exchange,
                    type: ExchangeType.Direct,
                    durable: true);
            }
        }

        public Task PublishMessage<T>(T message)
        {
            using var con = _connectionFactory.CreateConnection();
            if (con.IsOpen)
            {
                using var channel = con.CreateModel();
                var body = JsonSerializer.Serialize(message);
                var bytes = Encoding.UTF8.GetBytes(body);

                channel.BasicPublish(
                    exchange: _exchange,
                    routingKey: _routingKey,
                    body: bytes);
            }
            return Task.CompletedTask;
        }
    }
}

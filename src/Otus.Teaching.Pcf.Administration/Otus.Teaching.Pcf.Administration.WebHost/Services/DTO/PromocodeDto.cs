﻿using System;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services.DTO
{
    public class PromocodeDto
    {
        public Guid PartnerManagerId { get; set; }
    }
}

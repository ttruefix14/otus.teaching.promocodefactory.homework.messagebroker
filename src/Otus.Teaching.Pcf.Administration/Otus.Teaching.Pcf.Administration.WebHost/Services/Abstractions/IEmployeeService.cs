﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services.Abstractions
{
    public interface IEmployeeService
    {
        Task UpdateAppliedPromocodes(Guid id);
    }
}

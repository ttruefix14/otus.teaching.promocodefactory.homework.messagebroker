﻿using Microsoft.AspNetCore.Connections;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Options;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services.Abstractions;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.HostedServices
{
    public class PromocodeConsumerService : BackgroundService
    {
        private readonly IPromocodeService _promocodeService;
        private readonly string _exchange = "Pcf.ReceivingFromPartner.Promocodes";
        private readonly string _queue = "Pcf.GivingToCustomer.Promocodes";
        private readonly string _routingKey = "Pcf.ReceivingFromPartner.Promocode";


        private readonly ConnectionFactory _connectionFactory;
        private IModel _channel;
        private IConnection _con;

        public PromocodeConsumerService(IOptions<BusOptions> busOptions, IPromocodeService promocodeService)
        {
            _promocodeService = promocodeService;
            _connectionFactory = new()
            {
                UserName = busOptions.Value.Username,
                Password = busOptions.Value.Password,
                HostName = busOptions.Value.Host,
                Port = busOptions.Value.Port,
                VirtualHost = busOptions.Value.VirtualHost
            };
        }

        public IModel InitConsumer()
        {
            _con = _connectionFactory.CreateConnection();
            _channel = _con.CreateModel();
            _channel.QueueDeclare(
                queue: _queue,
                exclusive: false,
                durable: true,
                autoDelete: false);

            // Необязательно создавать exchange
            _channel.ExchangeDeclare(
                exchange: _exchange,
                type: ExchangeType.Direct,
                durable: true);

            _channel.QueueBind(
                queue: _queue,
                exchange: _exchange,
                routingKey: _routingKey);

            return _channel;
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            return base.StopAsync(cancellationToken);
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {

            var _channel = InitConsumer();
            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += async (s, ea) =>
            {

                var message = Encoding.UTF8.GetString(ea.Body.ToArray());
                var promocode = JsonSerializer.Deserialize<GivePromoCodeRequest>(message);
                await _promocodeService.GivePromoCodesToCustomersWithPreferenceAsync(promocode);

                _channel.BasicAck(ea.DeliveryTag, false);
            };
            //  consumer.ConsumerCancelled += Consumer_ConsumerCancelled;
            _channel.BasicConsume(queue: _queue, autoAck: false, consumer: consumer);

            return Task.CompletedTask;
        }
    }
}

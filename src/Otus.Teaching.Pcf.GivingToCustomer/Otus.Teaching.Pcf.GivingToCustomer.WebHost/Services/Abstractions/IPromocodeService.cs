﻿using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services.Abstractions
{
    public interface IPromocodeService
    {
        Task GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request);
    }
}
